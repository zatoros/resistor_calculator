﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace resistor_calculator
{
    /// <summary>
    /// Main window class
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Helper emum
        /// </summary>
        public enum BandNumberMode { Bands4, Bands5, Bands6 };

        /// <summary>
        /// List with resistance value prefixes( k - kilom, M - mega etc...)
        /// </summary>
        public static readonly List<string>  PREFIXES = new List<string>() { "", "k", "M", "G" };

        /// <summary>
        /// Binding collection for Eseries combobox
        /// </summary>
        public List<Data.Eseries> EseriesList { set; get; }

        #region Binding collections for Band comboboxes

        /// <summary>
        /// Binding Base collection for Band comboboxes
        /// </summary>
        public List<Data.ResistorBand> ResistorBands { set; get; }

        public List<Data.ResistorBand> ResistorBandsBasic 
        {
            get
            {
                return ResistorBands.Where(p => p.Digit != Data.ResistorBand.EMPTY_COLOR).ToList();
            }
        }

        public List<Data.ResistorBand> ResistorBandsMultiplier 
        { 
            get
            {
                return ResistorBands.Where(p => p.Multiplier != Data.ResistorBand.EMPTY_COLOR).ToList();
            }
        }

        public List<Data.ResistorBand> ResistorBandsTolerance 
        { 
            get
            {
                return ResistorBands.Where(p => p.Tolerance != Data.ResistorBand.EMPTY_COLOR).ToList();
            }
        }

        public List<Data.ResistorBand> ResistorBandsTemperature
        {
            get
            {
                return ResistorBands.Where(p => p.Tempco != Data.ResistorBand.EMPTY_COLOR).ToList();
            }
        }

        #endregion Binding collections for Band comboboxes

        #region Properties

        /// <summary>
        /// Band number node based on combobox values
        /// </summary>
        public BandNumberMode CurrentBandNumberMode
        {
            get
            {
                if (cb4Bands.IsChecked.Value)
                {
                    return BandNumberMode.Bands4;
                }
                else if (cb5Bands.IsChecked.Value)
                {
                    return BandNumberMode.Bands5;
                }
                else
                {
                    return BandNumberMode.Bands6;
                }
            }
        }

        public int? Digit1 { get; set; }
        public int? Digit2 { get; set; }
        public int? Digit3 { get; set; }
        public int? Multiplier { get; set; }
        public double? Tolerance { get; set; }
        public int? Tempco { get; set; }

        public Data.Eseries Eseries { get; set; }

        #region Display properties

        public string ConvertToResitanceString(long resistance)
        {
            int index = 0;
            double x = resistance;
            while (x >= 1000)
            {
                x /= 1000;
                ++index;
            }
            return x.ToString() + " " + PREFIXES[index] + "Ω";
        }

        public long? ResistanceDisplay
        {
            set
            {
                if (value.HasValue)
                {
                    tbResistance.Text = ConvertToResitanceString(value.Value);
                }
                else
                {
                    tbResistance.Text = string.Empty;
                }
            }
        }

        public double? ToleranceDisplay
        {
            set
            {
                if (value.HasValue)
                {
                    tbTolerance.Text = value.Value.ToString() + " %";
                }
                else
                {
                    tbTolerance.Text = string.Empty;
                }
            }
        }

        public int? TempcoDisplay
        {
            set
            {
                if (value.HasValue)
                {
                    tbTempco.Text = value.Value.ToString() + " ppm/°C";
                }
                else
                {
                    tbTempco.Text = string.Empty;
                }
            }
        }

        public long? ClosestValueDisplay
        {
            set
            {
                if (value.HasValue)
                {
                    tbClosestValue.Text = ConvertToResitanceString(value.Value);
                }
                else
                {
                    tbClosestValue.Text = string.Empty;
                }
            }
        }

        public double? ErrorDisplay
        {
            set
            {
                if (value.HasValue)
                {
                    tbErrorValue.Text = value.Value.ToString("n4") + " %";
                }
                else
                {
                    tbErrorValue.Text = string.Empty;
                }
            }
        }

        #endregion Display properties

        #endregion Properties

        public MainWindow()
        {
            this.DataContext = this;
            ResistorBands = new List<Data.ResistorBand>();
            ResistorBands.AddRange(Data.ResistorBand.LoadResistorBands());
            EseriesList = new List<Data.Eseries>();
            EseriesList.AddRange(Data.Eseries.LoadEseriesList());

            InitializeComponent();
        }

        #region Event Handlers

        #region Check Box event handlers

        /// <summary>
        /// Helper function enabling\disabling checboxes events
        /// </summary>
        /// <param name="enable"></param>
        private void EnableCbBandsEvents(bool enable)
        {
            if (enable)
            {
                cb4Bands.Checked += cb4Bands_Checked;
                cb5Bands.Checked += cb5Bands_Checked;
                cb6Bands.Checked += cb6Bands_Checked;
                cb4Bands.Unchecked += cbBands_Unchecked;
                cb5Bands.Unchecked += cbBands_Unchecked;
                cb6Bands.Unchecked += cbBands_Unchecked;
            }
            else
            {
                cb4Bands.Checked -= cb4Bands_Checked;
                cb5Bands.Checked -= cb5Bands_Checked;
                cb6Bands.Checked -= cb6Bands_Checked;
                cb4Bands.Unchecked -= cbBands_Unchecked;
                cb5Bands.Unchecked -= cbBands_Unchecked;
                cb6Bands.Unchecked -= cbBands_Unchecked;
            }
        }

        private void cb4Bands_Checked(object sender, RoutedEventArgs e)
        {
            EnableCbBandsEvents(false);
            cb5Bands.IsChecked = false;
            cb6Bands.IsChecked = false;
            BandDigit3.Visibility = System.Windows.Visibility.Hidden;
            BandTempco.Visibility = System.Windows.Visibility.Hidden;
            EnableCbBandsEvents(true);
            DisplayResults();
        }

        private void cb5Bands_Checked(object sender, RoutedEventArgs e)
        {
            EnableCbBandsEvents(false);
            cb4Bands.IsChecked = false;
            cb6Bands.IsChecked = false;
            BandDigit3.Visibility = System.Windows.Visibility.Visible;
            BandTempco.Visibility = System.Windows.Visibility.Hidden;
            EnableCbBandsEvents(true);
            DisplayResults();
        }

        private void cb6Bands_Checked(object sender, RoutedEventArgs e)
        {
            EnableCbBandsEvents(false);
            cb4Bands.IsChecked = false;
            cb5Bands.IsChecked = false;
            BandDigit3.Visibility = System.Windows.Visibility.Visible;
            BandTempco.Visibility = System.Windows.Visibility.Visible;
            EnableCbBandsEvents(true);
            DisplayResults();
        }

        private void cbBands_Unchecked(object sender, RoutedEventArgs e)
        {
            EnableCbBandsEvents(false);
            CheckBox cb = (CheckBox)sender;
            cb.IsChecked = true;
            EnableCbBandsEvents(true);
        }

        #endregion Check Box event handlers

        /// <summary>
        /// Function displaing band-based values
        /// </summary>
        private void DisplayResults()
        {
            long? resistance = null;
            double? tolerance = Tolerance;
            int? tempco = CurrentBandNumberMode == BandNumberMode.Bands6 ? Tempco : null;

            switch (CurrentBandNumberMode)
            {
                case BandNumberMode.Bands4:
                    {
                        if (Digit1.HasValue && Digit2.HasValue && Multiplier.HasValue)
                        {
                            resistance = (long)((Digit1.Value * 10 + Digit2.Value) * Math.Pow(10, Multiplier.Value));
                        }
                    }
                    break;
                case BandNumberMode.Bands5:
                case BandNumberMode.Bands6:
                    {
                        if (Digit1.HasValue && Digit2.HasValue && Digit3.HasValue && Multiplier.HasValue)
                        {
                            resistance = (long)((Digit1.Value * 100 + Digit2.Value * 10 + Digit3) * Math.Pow(10, Multiplier.Value));
                        }
                    }
                    break;
            }

            ResistanceDisplay = resistance;
            TempcoDisplay = tempco;
            ToleranceDisplay = tolerance;
        }

        #region Band combo boxes event handlers

        private void BandDigit1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Data.ResistorBand temp = e.AddedItems.Count > 0 ? (Data.ResistorBand)e.AddedItems[0] : null;
            if (temp != null)
            {
                Digit1 = temp.Digit;
            }
            else
            {
                Digit1 = null;
            }
            DisplayResults();
        }

        private void BandDigit2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Data.ResistorBand temp = e.AddedItems.Count > 0 ? (Data.ResistorBand)e.AddedItems[0] : null;
            if (temp != null)
            {
                Digit2 = temp.Digit;
            }
            else
            {
                Digit2 = null;
            }
            DisplayResults();
        }

        private void BandDigit3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Data.ResistorBand temp = e.AddedItems.Count > 0 ? (Data.ResistorBand)e.AddedItems[0] : null;
            if (temp != null)
            {
                Digit3 = temp.Digit;
            }
            else
            {
                Digit3 = null;
            }
            DisplayResults();
        }

        private void BandMultiplier_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Data.ResistorBand temp = e.AddedItems.Count > 0 ? (Data.ResistorBand)e.AddedItems[0] : null;
            if (temp != null)
            {
                Multiplier = temp.Multiplier;
            }
            else
            {
                Multiplier = null;
            }
            DisplayResults();
        }

        private void BandTolerance_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Data.ResistorBand temp = e.AddedItems.Count > 0 ? (Data.ResistorBand)e.AddedItems[0] : null;
            if (temp != null)
            {
                Tolerance = temp.Tolerance;
            }
            else
            {
                Tolerance = null;
            }
            DisplayResults();
        }

        private void BandTempco_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Data.ResistorBand temp = e.AddedItems.Count > 0 ? (Data.ResistorBand)e.AddedItems[0] : null;
            if (temp != null)
            {
                Tempco = temp.Tempco;
            }
            else
            {
                Tempco = null;
            }
            DisplayResults();
        }

        #endregion Band combo boxes event handlers

        private void Eseries_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Eseries = e.AddedItems.Count > 0 ? (Data.Eseries)e.AddedItems[0] : null;
        }

        private void Calculate_Click(object sender, RoutedEventArgs e)
        {
            long resistance;
            if (long.TryParse(tbResistanceToFind.Text, out resistance) && Eseries != null)
            {
                long foundResistance = Eseries.GetBestFitValue(resistance);
                ClosestValueDisplay = foundResistance;
                ErrorDisplay = ((double)(resistance - foundResistance) / (resistance)) * 100;
            }
        }

        #endregion Event Handlers
    }
}
